﻿Import-Module ActiveDirectory
Import-Csv "C:\uSERS\Gema Abriantini\Documents\IDN2017\USERLIST-IT.csv" | ForEach-Object {
 $userPrincinpal = $_."samAccountName" + "@IDNCompetition2017.id"
New-ADUser -Name $_.Name `
 -Path $_."ParentOU" `
 -SamAccountName  $_."samAccountName" `
 -UserPrincipalName  $userPrincinpal `
 -AccountPassword (ConvertTo-SecureString "Idn123$%" -AsPlainText -Force) `
 -ChangePasswordAtLogon $false  `
 -Enabled $true
Add-ADGroupMember "IDN-ITUser" $_."samAccountName";
}